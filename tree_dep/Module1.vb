﻿Imports System.IO
Imports System.Text.RegularExpressions
Module Module1
    Dim files As New List(Of String)
    Dim fileNames As New List(Of String)
    Dim traversedFrom As New List(Of String)
    Dim hasLine As New List(Of Boolean)
    Dim argv() As String = System.Environment.GetCommandLineArgs()
    Dim sw As StreamWriter = My.Computer.FileSystem.OpenTextFileWriter("dep.txt", False)
    Sub Main()
        If argv.Count < 2 Or argv.Contains("/?") Or argv.Contains("-?") Or argv.Contains("--help") Then
            Console.WriteLine("Command line arguments:")
            Console.WriteLine("  The first argument must be a filename, or a * to analyse all files in a directory")
            Console.WriteLine("The following switches may follow:")
            Console.WriteLine(" -f: Flat; do not recurse into called files")
            Console.WriteLine(" -r: Show all repeated inclusions ")
            Console.WriteLine(" -df: Display Filenames of all files being searched through")
            Console.WriteLine(" -pt: If a file has already been traversed, print this in a new line")
        Else
            For Each f As FileInfo In New DirectoryInfo(Directory.GetCurrentDirectory).GetFiles()
                If {".f90", ".asm"}.Contains(f.Extension.ToLower) Then
                    files.Add(My.Computer.FileSystem.ReadAllText(f.FullName))
                    fileNames.Add(f.Name)
                    traversedFrom.Add("")
                    If argv.Contains("-df") Then Console.Write(f.Name & vbTab)
                End If
            Next
            out("BEGIN " & argv(1))
            If argv(1) = "*" Then
                For i = 0 To files.Count - 1
                    out("BEGIN " & fileNames(i))
                    traverse(i, 0, "root")
                Next
            Else
                traverse(fileNames.IndexOf(argv(1)), 0, "root")
            End If
        End If
        sw.Close()
    End Sub
    Sub traverse(fId As Integer, depth As Integer, traversedBy As String)
        'out(getDrops(depth) & fileNames(fId) & " - " & depth & " - " & traversedBy & ":" & traversedFrom(fId))
        hasLine.Add(True)
        If traversedFrom(fId) = "" Then traversedFrom(fId) = traversedBy
        'If depth > 2 Then Exit Sub
        'If depth > 0 Then
        '    Console.
        'End If
        Dim ittd = {({"USE", "^[^\S\n]*USE[^\S\n]+(?<mod>[A-Z_0-9]+)\r?\n?$", "^[^\S\n]*MODULE[^\S\n]+(?<mod>#ID#)\r?\n?$", ".f90"}),
                    ({"EXTRN", "^[^\S\n]*EXTRN[^\S\n]+(?<mod>[A-Z_0-9]+)(:[A-Z_0-9]+)?\r?\n?$", "^[^\S\n]*(COMM|PUBLIC)[^\S\n]+(?<mod>#ID#)(:[A-Z_0-9]+)?(:[0-9]+)?\r?\n?$", ".asm"}),
                    ({"SUBROUTINE", "^[^\S\n]*INTERFACE[^\S]+SUBROUTINE[^\S\n](?<mod>[A-Z_0-9]+) ?\(.*\)\r?\n?$", "^[^\S\n]?(SUBROUTINE)[^\S\n]+(?<mod>#ID#)\(.*\)\r?\n?$", ".f90"})}
        Dim ID(ittd.Count - 1) As List(Of String)
        Dim inFile(ittd.Count - 1) As List(Of String)
        Dim n = -1
        For Each ls In ittd
            Dim alreadyUsed As New List(Of String)
            n += 1
            ID(n) = New List(Of String)
            inFile(n) = New List(Of String)
            'Console.WriteLine(ls(0) & ":" & ls(1))
            'out(files(fileNames.IndexOf(fName)))
            Dim r As New Regex(ls(1), RegexOptions.IgnoreCase Or RegexOptions.Multiline Or RegexOptions.Compiled) '^[^\S\n]*USE[^\S\n]+([A-Z_0-9]+)$
            For Each m As Match In r.Matches(files(fId))
                If Not argv.Contains("-r") AndAlso alreadyUsed.Contains(m.Groups("mod").Value) Then Continue For
                ID(n).Add(m.Groups("mod").Value)
                alreadyUsed.Add(m.Groups("mod").Value)
                inFile(n).Add("<missing>")
                Dim rm As New Regex(ls(2).Replace("#ID#", ID(n)(ID(n).Count - 1)), RegexOptions.IgnoreCase Or RegexOptions.Multiline Or RegexOptions.Compiled) '^[^\S\n]*USE[^\S\n]+([A-Z_0-9]+)$
                For i = 0 To files.Count - 1
                    If Not fileNames(i).EndsWith(ls(3)) Then Continue For
                    If rm.IsMatch(files(i)) Then
                        inFile(n)(ID(n).Count - 1) = fileNames(i)
                        'out(rm.Match(files(i)).Value)
                        Exit For
                    ElseIf i = files.Count - 1 Then
                        'inFile(inFile.Count - 1) = "<missing>"
                    End If
                Next
            Next
            'If ID.Count = 0 Then Console.Write("*")
            'out()
        Next
        For n = 0 To ittd.Count - 1
            For i = 0 To ID(n).Count - 1
                Dim isend = i = ID(n).Count - 1
                For j = n + 1 To ittd.Count - 1
                    If ID(j).Count > 0 Then isend = False
                Next
                hasLine(depth) = Not isend
                out(getDrops(depth) & If(isend, "└─", "├─") & ittd(n)(0) & " " & ID(n)(i) & " <" & inFile(n)(i) & ">" & If(fileNames.Contains(inFile(n)(i)) AndAlso traversedFrom(fileNames.IndexOf(inFile(n)(i))) <> "", " *[" & traversedFrom(fileNames.IndexOf(inFile(n)(i))) & "]", If(inFile(n)(i).StartsWith("<"), "", "")))
                If Not inFile(n)(i).StartsWith("<") Then
                    If traversedFrom(fileNames.IndexOf(inFile(n)(i))) = "" Then
                        If Not argv.Contains("-f") Then traverse(fileNames.IndexOf(inFile(n)(i)), depth + 1, fileNames(fId))
                    Else
                        If argv.Contains("-pt") Then out(getDrops(depth + 1) & "└─* [" & traversedFrom(fileNames.IndexOf(inFile(n)(i))) & "]")
                    End If
                Else
                    'out()
                End If
                'out()
            Next
        Next
        hasLine.RemoveAt(hasLine.Count - 1)
    End Sub
    Function getDrops(n As Integer) As String
        Dim r As New String("")
        For i = 0 To n - 1
            r &= If(hasLine(i), "│ ", "  ")
        Next
        Return r
    End Function
    Sub out(s As String)
        Console.WriteLine(s)
        sw.WriteLine(s)
    End Sub
End Module
